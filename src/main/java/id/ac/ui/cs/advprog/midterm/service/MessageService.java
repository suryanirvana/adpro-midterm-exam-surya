package id.ac.ui.cs.advprog.midterm.service;

import id.ac.ui.cs.advprog.midterm.entity.Message;
import java.util.List;

public interface MessageService {

    List<Message> getAllMessages();

    void addMessage(Message message);

    void removeMessageById(long id);

    Message getMessageById(long id);
}
