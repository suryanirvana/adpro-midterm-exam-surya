package id.ac.ui.cs.advprog.midterm.service;

import id.ac.ui.cs.advprog.midterm.entity.Message;
import id.ac.ui.cs.advprog.midterm.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService{

    @Autowired
    MessageRepository messageRepository;

    @Override
    public List<Message> getAllMessages() {
        return messageRepository.findAllByOrderByIdAsc();
    }

    @Override
    public void addMessage(Message message) {
        messageRepository.save(message);
    }

    @Override
    public void removeMessageById(long id) {
        messageRepository.deleteById(id);
    }

    @Override
    public Message getMessageById(long id) {
        if(!messageRepository.existsById(id)) {
            throw new EntityNotFoundException();
        }
        return messageRepository.findById(id);
    }
}
