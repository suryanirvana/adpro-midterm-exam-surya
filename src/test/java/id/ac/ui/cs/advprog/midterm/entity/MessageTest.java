package id.ac.ui.cs.advprog.midterm.entity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class MessageTest {

    @Autowired
    private Message message = new Message();
    private Date date = new Date(0);

    @BeforeEach
    void setUpMessage() {
        message = new Message();
        message.setId(1);
        message.setText("This is a message");
        message.setDate(date);
    }

    @Test
    void checkIfIdEqualsAndExists() {
        assertEquals(1, message.getId());
        assertNotEquals(0, message.getId());
    }

    @Test
    void checkIfMessageEqualsAndExists() {
        assertEquals("This is a message", message.getText());
        assertNotNull(message.getText());
    }

    @Test
    void checkIfDateEqualsAndExists() {
        assertEquals(date, message.getDate());
        assertNotNull(message.getDate());
    }
}
