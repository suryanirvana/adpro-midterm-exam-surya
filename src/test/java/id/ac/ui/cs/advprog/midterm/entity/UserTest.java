package id.ac.ui.cs.advprog.midterm.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

public class UserTest {

    @Autowired
    private static User user;

    @BeforeEach
    void setUpUser() {
        user = new User();
        user.setId(8080);
        user.setName("Surya");
        user.setEmail("abc@xyz.com");
    }
    @Test
    void checkIfNameEqualsAndExists() {
        assertEquals("Surya", user.getName());
        assertNotNull(user.getName());
    }

    @Test
    void checkIfEmailEqualsAndExists() {
        assertEquals("abc@xyz.com", user.getEmail());
        assertNotNull(user.getEmail());
    }

    @Test
    void checkIfIdEqualsAndExists() {
        assertEquals(8080, user.getId());
    }

    @Test
    void checkToStringMethod() {
        assertEquals("User{id=8080, name='Surya', email='abc@xyz.com'}", user.toString());
    }
}
